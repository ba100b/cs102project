package city_route.core;

import city_route.interfaces.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;


public class CountryTest {
    public static void main(String[] args) throws IOException {


        IPath route1 = new Route();
        CityImpl jeddah = new CityImpl("Jeddah", new GPS(21.2854, 39.2376));
        ICity makkah = new CityImpl("Makkah", new GPS(21.3891, 39.8579));
        ICity madinah = new CityImpl("Madinah", new GPS(24.5247, 39.5692));
        ICity riyadh = new CityImpl("Riyadh", new GPS(24.7136, 46.6753));

        Route route2 = new Route();

        System.out.println("Route Length: " + route1.getLength());
        route1.addCityToPath(jeddah);
        route1.addCityToPath(makkah);
        route1.addCityToPath(madinah);
        route1.addCityToPath(riyadh);
        System.out.println("Route Length: " + (float)route1.getLength());
        System.out.println(route1);

        for (ICity city : route1.getPath()) {
            System.out.println(city);
        }

        File file = new File("CITIES.txt");
        FileWriter fw = new FileWriter(file, true);

        for (ICity c : route1.getPath()) {
            String s = c.getName() + ";";
            s = s + ";" + c.getLocation3D().getLat() + ";" + c.getLocation3D().getLon() + ";" + c.getLocation3D().getAltitude();
            s = s + "\n";
            fw.write(s);
        }
        fw.close();


        ArrayList<IPath> routes = new ArrayList<IPath>();
        routes.add(route1);
        routes.add(route2);
        System.out.println(Collections.max(routes));

    }
}
