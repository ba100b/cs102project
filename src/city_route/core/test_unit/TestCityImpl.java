package city_route.core.test_unit;

import city_route.core.CityImpl;
import city_route.core.GPS;
import city_route.interfaces.ICity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ub on 22/12/16.
 */
public class TestCityImpl {

    public static void main(String[] args) {
        GPS gps1 = new GPS(30.0, 6.0, 1);
        GPS gps2 = new GPS(30.0, 6.0, 1);
        ICity city1 = new CityImpl("Jeddah1", gps1);
        ICity city2 = new CityImpl("Amman", gps2);
        ICity city3 = new CityImpl("Riyadh", gps2);

        System.out.println(city1.equals(city2));

        List<ICity> cities = new ArrayList<>();
        cities.add(city1);
        cities.add(city2);
        System.out.println("City1 index: " +cities.indexOf(city1));
        System.out.println("City2 index: " +cities.indexOf(city2));
        System.out.println("City3 index: " + cities.indexOf(city3));
        System.out.println("is city2 in the list ? " + cities.contains(city2));
    }

}
