package city_route.core;

import city_route.interfaces.*;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Route implements IPath, Comparator<Route> {

    private static int code = 10;
    private List<ICity> citiesRoute = new LinkedList<>();

    public List<ICity> getCitiesRoute() {
        return citiesRoute;
    }

    @Override
    public void setCitiesRoute(List<ICity> citiesRoute) {
        this.citiesRoute = citiesRoute;
    }

    @Override
    public void addCityToPath(ICity city) {
        citiesRoute.add(city);
    }

    @Override
    public void removeCityFromPath(ICity city) {
        citiesRoute.remove(city);
    }

    @Override
    public double getLength() {
        double len = 0.0;
        for (int i = 0; i < citiesRoute.size() - 1; i++) {
            if(citiesRoute.get(i)!= null && citiesRoute.get(i + 1) != null) {
                GPS gps1 = (GPS) citiesRoute.get(i).getLocation3D();
                GPS gps2 = (GPS) citiesRoute.get(i + 1).getLocation3D();
                len = len + GPSDistanceCalculator.distance(gps1, gps2, "K");
            }
        }
        return len;
    }

    @Override
    public List<ICity> getPath() {
        return citiesRoute;
    }

    @Override
    public ICoordinate3D getStartLocation() {
        return getPath().get(0).getLocation3D();
    }

    @Override
    public ICoordinate3D getDestinationLocation() {
        return getPath().get(citiesRoute.size() - 1).getLocation3D();
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < citiesRoute.size(); i++) {
            ICity city = citiesRoute.get(i);
            if(city != null) {
                if (i < citiesRoute.size() - 1)
                    s += city.getName() + " >> ";
                else
                    //don't add '>>' to the last element
                    s += city.getName();
            }
        }
        return String.format("%s", s);
    }

    /**
     * Compare between the length of two paths
     */
    @Override
    public int compareTo(IPath route) {
        if (this.getLength() > route.getLength()) {
            return 1;
        } else if (this.getLength() < route.getLength()) {
            return -1;
        }
        return 0;
    }

    @Override
    public int compare(Route o1, Route o2) {
        return o1.compareTo(o2);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Route) {
            return this.compareTo((Route)o) == 0;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return code;
    }
}
