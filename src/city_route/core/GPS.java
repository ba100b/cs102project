package city_route.core;

import city_route.interfaces.*;


public class GPS implements ICoordinate2D, ICoordinate3D {

    private static final double ALT = 1.0;
    private double lat; //x
    private double lon; //y
    private double altitude; //z

    public GPS() {
    }

    /**
     * 3D Coordinate
     */
    public GPS(double lat, double lon, double altitude) {
        setLat(lat);
        setLon(lon);
        setAltitude(altitude);
    }

    /**
     * 2D Coordinate
     */
    public GPS(double lat, double lon) {
        this(lat, lon, ALT);
    }


    @Override
    public double getLat() {
        return lat;
    }

    @Override
    public void setLat(double latitude) {
        if ((latitude > -85) && (latitude < 85))
            this.lat = latitude;
        else
            throw new IllegalArgumentException("Wrong latitude");
    }

    @Override
    public double getLon() {
        return lon;
    }

    @Override
    public void setLon(double longitude) {
        if ((longitude > -180) && (longitude < 180))
            this.lon = longitude;
        else
            throw new IllegalArgumentException("Wrong longitude");
    }

    @Override
    public double getAltitude() {
        return altitude;
    }

    @Override
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }


    @Override
    public String toString() {
        return "GPS{" + "lat=" + lat + ", lon=" + lon + '}';
    }


}
