package city_route.core;

import city_route.interfaces.*;
import parse_cities_coordinates_file.Country;
import parse_cities_coordinates_file.ParseAndList;

import java.util.Arrays;
import java.util.List;


public class CityImpl implements ICity {
    private String name;
    private ICoordinate3D location3D;
    private ICoordinate2D location2D;

    /**
     * Three Dimensions constructor
     */
    public CityImpl(String name, ICoordinate3D location3D) {
        this.name = name;
        this.location3D = location3D;
    }

    /**
     * Two Dimensions constructor
     */
    public CityImpl(String name, ICoordinate2D location2D) {
        this.name = name;
        this.location2D = location2D;
    }

    /**
     * empty constructor
     */
    public CityImpl() {
    }

    /**
     * copy constructor of classs CityImpl
     *
     * @param city: the city you want to copy its content
     */
    public CityImpl(CityImpl city) {
        this(city.name, city.location3D);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {

        boolean contains = false;
        for (String cityName : ParseAndList.getInstance().getCitiesNames()) {
            if (name.equalsIgnoreCase(cityName)) {
                this.name = cityName;
                contains = true;
            }
        }
        if (!contains) {
            throw new IllegalArgumentException("name not found");
        }
    }

    public ICoordinate3D getLocation3D() {
        return location3D;
    }

    public void setLocation3D(ICoordinate3D location3D) {
        this.location3D = location3D;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ICity)
            return getName().equals(((CityImpl) o).getName());
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        //+ ", location3D=" + location3D +
        return "City { " + name + " }";
    }

}
