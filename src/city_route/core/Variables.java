package city_route.core;

import parse_cities_coordinates_file.Country;

/**
 * Created by ub on 16/12/16.
 */
public final class Variables {

    /**
     * location of the file
     */
    public final static String FILE_NAME = "src/files/CitiesCoordinates.txt";
    /**
     * can't exceed this number of loops
     */
    public final static int MAX_LOOPS = 2000000;
    /**
     * number of times the loop will continue
     */
    public static final int LOOPS_NUMBER = 1000;

    /**
     * represent the number of cities in the list
     * (+1) because class Country doesn't contains destination city
     */
    public static final int NUMBER_OF_CITIES = Country.numberOfCities + 1;


}
