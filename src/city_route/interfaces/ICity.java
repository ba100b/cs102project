package city_route.interfaces;


public interface ICity {

    String getName();

    void setName(String name);

    ICoordinate3D getLocation3D();

    void setLocation3D(ICoordinate3D location3D);
}
