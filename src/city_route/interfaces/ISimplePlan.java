package city_route.interfaces;

import city_route.core.Route;

import java.util.List;
import java.util.Set;

/**
 * Created by ub on 24/12/16.
 */
public interface ISimplePlan {

    /**
     * @param crossLoops: represent how many times we will do cross tours
     */
    Set<Route> crossTours(int crossLoops);

    /**
     * This method include segment mutation and cities mutation
     * @param routeArrayList represent the list that mutation will be applied on its routes
     */
    Set<Route> segmentAndCitiesMutation(List<Route> routeArrayList);

    /**
     * @param s represents how the number should be generated
     */
    int generateNumberM(String s);

}
