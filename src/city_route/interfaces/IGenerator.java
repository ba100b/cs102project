package city_route.interfaces;

import city_route.core.Route;
import city_route.interfaces.ICity;
import java.util.Set;

/**
 * Created by ub on 19/12/16.
 */
public interface IGenerator {

    /**
     * @return the default route depending on the file (CitiesCoordinates.txt) contents order
     */
    Route getDefaultRoute();

    /**
     * Make a city become the first in the list and the last in the route
     */
    void makeCitySrcDist(Route route, ICity city);

    /**
     * @return set of generated tours
     */
    Set<Route> generateTours(int numberOfLoops);

}
