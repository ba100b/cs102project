package city_route.interfaces;

import city_route.core.CityImpl;
import city_route.interfaces.ICity;

import java.io.File;
import java.util.List;

/**
 * Created by ub on 15/12/16.
 */
public interface IParse {

    //getters and setters
    //-----------------------------------

    String getFileName();

    void setFileName(String fileName);

    void setCity(CityImpl city);

    List<ICity> getCities();

    void setCities(List cities);

    /**
     * get the File of CITIES coordinates
     */
    File getCitiesCoordinatesFile();

    /**
     * Create File to read from
     */
    void setCitiesCoordinatesFile(File citiesCoordinatesFile);

    //------------------------------------
    //other methods in order of execution

    /**
     * Create a File in order to read CitiesCoordinates.txt
     */
    void createFileToRead(String fileName);

    /**
     * create scanner that read the file passed by the argument
     *
     * @param fileToRead
     */
    void makeReaderScanner(File fileToRead);

    /**
     * get one city from one line in the file, and return null if the file finish
     */
    CityImpl getCityFromFile();

    /**
     * @return the whole CITIES in the file in a list of CITIES
     */
    List<ICity> getAllCitiesFromFile();

    /**
     * re-instantiate scanner in order to read from the beginning
     */
    void resetScanner();

    /**
     * Instantiate the attribute
     *
     * @param fileName: pass file name
     */
    void init(String fileName);

}
