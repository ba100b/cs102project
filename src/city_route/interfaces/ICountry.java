package city_route.interfaces;

/**
 * Created by ub on 24/12/16.
 */
public interface ICountry {

    /**Select the city you want to set off from*/
    boolean setTargetCity(String cityName);

}
