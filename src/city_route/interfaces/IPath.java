package city_route.interfaces;

import java.util.List;


public interface IPath extends Comparable<IPath> {

    /**
     * Gets the path as a List of CITIES
     */
    List<ICity> getPath();

    /**
     * Get the length of the path
     */
    double getLength();

    /**
     * Returns the coordinate of the start location of the path
     */
    ICoordinate3D getStartLocation();

    /**
     * Returns the coordinate of the destination location of the path
     */
    ICoordinate3D getDestinationLocation();


    void addCityToPath(ICity city);

    void removeCityFromPath(ICity city);

    /**
     * @return list of Cities
     */
    List<ICity> getCitiesRoute();

    /**
     * @param citiesRoute will set its value to the list of CITIES in Route object
     */
    void setCitiesRoute(List<ICity> citiesRoute);


}
