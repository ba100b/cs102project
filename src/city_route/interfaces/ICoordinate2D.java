package city_route.interfaces;


public interface ICoordinate2D {

    /**
     * It is X
     */
    double getLat();

    /**
     * It is X
     */
    void setLat(double x);

    /**It is Y*/
    double getLon();

    /**It is Y*/
    void setLon(double y);

}
