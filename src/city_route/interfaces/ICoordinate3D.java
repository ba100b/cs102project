package city_route.interfaces;

public interface ICoordinate3D extends ICoordinate2D {

    /**
     * It is Z
     */
    double getAltitude();

    /**
     * It is Z
     */
    void setAltitude(double z);
}
