package tour_plans.plans;

import city_route.core.Route;
import city_route.core.Variables;
import city_route.interfaces.ICity;
import city_route.interfaces.ISimplePlan;
import tour_plans.generate_paths.RoutesGenerator;
import utils.MyUtils;

import java.util.*;

/**
 * Created by ub on 20/12/16.
 */
public class SimplePlan implements ISimplePlan {

    /**
     * store the result of generateTours and is used in mutation method
     */
    private List<Route> routeList;

    private static final String CITIES = "cities";
    private static final String ROUTES = "routes";
    private int toursNumber;

    /**
     * @param numberOfLoops: how many random generations is wanted
     */
    public SimplePlan(int numberOfLoops) {
        //because I don't want to check every time if there is a duplicate value
        //since I add to it many times in mutations method
        //will add it to Set in the end to check them just once time
        toursNumber = numberOfLoops;
    }

    public List<Route> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }


    public Set<Route> segmentAndCitiesMutation(List<Route> routeArrayList) {
        int randomM = generateNumberM(CITIES);
        int size = routeArrayList.size();
        for (int i = 0; i < size; i++) {
            Route route = new Route();
            route.getCitiesRoute().addAll(routeArrayList.get(i).getCitiesRoute());

            //segmentMutation
            Collections.swap(route.getCitiesRoute(), randomM, randomM + 1);
            routeArrayList.add(route);

            route = new Route();
            route.getCitiesRoute().addAll(routeArrayList.get(i).getCitiesRoute());

            //citiesMutation
            Collections.swap(route.getCitiesRoute(), randomM, generateNumberM(CITIES));

            //don't add the new route to TreeSet each time, it is for performance sake
            //Just add them all at once after the loop
            routeArrayList.add(route);
        }

        return new TreeSet<>(routeArrayList);
    }


    public Set<Route> crossTours(int crossLoops) {
        //To simplify iteration and modification
        Set<Route> routeSet = new TreeSet<>(new RoutesGenerator().generateTours(toursNumber));

        List<Route> routeList;
        System.out.println("Crossing starts, wait...\n\n");

        for (int i = 0; i < crossLoops; i++) {

            routeList = new ArrayList<>(routeSet);
            segmentAndCitiesMutation(routeList);

            toursNumber = routeList.size() - 1;

            Route tour1 = new Route();
            tour1.getCitiesRoute().addAll(routeList.get(generateNumberM(ROUTES))
                    .getCitiesRoute());

            Route tour2 = new Route();
            tour2.getCitiesRoute().addAll(routeList.get(generateNumberM(ROUTES))
                    .getCitiesRoute());

            int randomNumber = generateNumberM(CITIES);

            //the two cities indexes in tour1 that we want to put tour3 in the same index
            int[] index1 = {randomNumber, randomNumber + 1};

            ICity city1 = tour1.getCitiesRoute().get(index1[0]);
            ICity city2 = tour1.getCitiesRoute().get(index1[1]);


            Route tour3 = new Route();
            MyUtils.fillDummy(tour3.getCitiesRoute());

            tour3.getCitiesRoute().set(index1[0], tour1.getCitiesRoute().get(index1[0]));
            tour3.getCitiesRoute().set(index1[1], tour1.getCitiesRoute().get(index1[1]));


            /*
            make sure the index is not at the index of the two cities in tour1
            make sure the cities you want to set in tour 3 is not
            the same as city1 or city2
             */
            int counter = 1;
            for (int j = 1; j < Variables.NUMBER_OF_CITIES - 1; j++) {
                if (!tour2.getCitiesRoute().get(j).equals(city1)
                        && !tour2.getCitiesRoute().get(j).equals(city2)) {
                    while (counter < Variables.NUMBER_OF_CITIES - 1) {
                        if (counter != index1[0] && counter != index1[1]) {
                            tour3.getCitiesRoute().set(counter, tour2.getCitiesRoute().get(j));
                            counter++;
                            break;
                        }
                        counter++;
                    }
                }
            }

            routeSet.add(tour3);
            //keep best five
            Iterator iterator = routeSet.iterator();
            int counter1 = 0;
            while (iterator.hasNext()) {
                iterator.next();
                if (counter1 > 4) {
                    iterator.remove();
                }
                counter1++;
            }
        }


        return routeSet;
    }


    public int generateNumberM(String s) {
        //don't include source >> 1
        int min = 1;
        int max;

        // make it equals NUMBER_OF_CITIES -1 because of the index
        // also, don't include destination >> -1, and because we will swap with

        //the destination(last element) as well >> -1 -1 -1= -3
        if (s.equalsIgnoreCase(CITIES)) {
            max = Variables.NUMBER_OF_CITIES - 3;
        } else
            max = toursNumber;
        return min + (int) (Math.random() * (max - min + 1));
    }

}
