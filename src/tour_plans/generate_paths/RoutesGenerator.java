package tour_plans.generate_paths;

import city_route.core.CityImpl;
import city_route.core.Route;
import city_route.interfaces.ICity;
import city_route.interfaces.IGenerator;
import parse_cities_coordinates_file.Country;

import java.util.*;

/**
 * Created by ub on 16/12/16.
 */
public class RoutesGenerator implements IGenerator {
    /*/**
     * @return start and destination city(Riyadh in our case)

    public ICity getFirstCity() {
        iParse.resetScanner();
        return iParse.getCityFromFile();
    }*/

    @Override
    public Route getDefaultRoute() {
        List<ICity> cities = Country.CITIES;
        Route route = new Route();
        route.setCitiesRoute(cities);
        //because we need riyadh two times
        //route.addCityToPath(getFirstCity());
        return route;
    }

    @Override
    public void makeCitySrcDist(Route route, ICity city) {
        if (city instanceof CityImpl) {
            route.getCitiesRoute().add(0, city);
            route.getCitiesRoute().add(city);
        }
    }

    @Override
    public Set<Route> generateTours(int numberOfLoops) {
        System.out.println("Generating starts, wait...\n");

        Route defaultRoute = getDefaultRoute();

        Route routeToShuffle = new Route();
        routeToShuffle.getCitiesRoute().addAll(defaultRoute.getCitiesRoute());

        //remove the city that is going to be source and destination in
        //order to let shuffle become faster
        routeToShuffle.getCitiesRoute().remove(0);

        //create list of routeList to store generated routes in it
        List<Route> routeList = new LinkedList<>();

        for (int i = 0; i < numberOfLoops; i++) {
            try {
                //Make new list inside the loop so that not the same reference is adding
                //to routeList all the time, and copy the values of routeToShuffle.getCitiesRoute()
                //to cities
                //pass defaultRoute here not the route you are passing to makeCitySrcDist()
                //otherwise, each time a new city will be adding to it
                List<ICity> cities = new LinkedList<>(routeToShuffle.getCitiesRoute());

                Route randomRoute = new Route();
                //don't use setCitiesRoute() because it will be a reference to cities
                randomRoute.getCitiesRoute().addAll(cities);

                Collections.shuffle(randomRoute.getCitiesRoute());

                //System.out.println("CITIES" + i);

                makeCitySrcDist(randomRoute, defaultRoute.getCitiesRoute().get(0));
                //TODO : don't print when execute with high number of loop for performance sake
                //System.out.printf("%s\n\n", randomRoute);
                routeList.add(randomRoute);
            } catch (Exception ex) {
                break;
            }
        }

        Set<Route> routeSet = new TreeSet<>(routeList);
        return routeSet;
    }

}