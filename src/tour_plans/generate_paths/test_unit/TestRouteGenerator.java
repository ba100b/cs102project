package tour_plans.generate_paths.test_unit;

import city_route.core.Route;
import city_route.core.Variables;
import parse_cities_coordinates_file.ParseAndList;
import tour_plans.generate_paths.RoutesGenerator;

import java.util.Iterator;

/**
 * Created by ub on 16/12/16.
 */
public class TestRouteGenerator {

    private static RoutesGenerator routesGenerator = new RoutesGenerator();

    public static void main(String[] args) {
        //getFirstCityTest();
        //parse.resetScanner();
        generateToursTest(5);
    }

    public static void getDefaultRouteTest() {
        System.out.println(routesGenerator.getDefaultRoute().getLength());
    }


    public static void generateToursTest(int numberOfLoops) {
        int counter = 0;
        for (Iterator<Route> iterator = routesGenerator.generateTours(numberOfLoops).iterator(); iterator.hasNext(); ) {
            Route route = iterator.next();
            System.out.printf("counter: %d, Length: %f\n%s\n\n", counter++,
                    route.getLength(),
                    route);

        }
        //System.out.println(route);
    }


}
