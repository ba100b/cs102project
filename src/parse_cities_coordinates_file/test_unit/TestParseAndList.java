package parse_cities_coordinates_file.test_unit;

import city_route.core.CityImpl;
import city_route.core.Variables;
import parse_cities_coordinates_file.ParseAndList;

/**
 * Created by ub on 15/12/16.
 */
public class TestParseAndList {

    private static ParseAndList parse = ParseAndList.getInstance();

    public static void main(String[] args) {
        parse.init(Variables.FILE_NAME);
        //gettersTest();
        //getCityFromFileTest();
        //getAllCitiesFromFileTest();
        getCitiesNamesTest();
    }

    private static void gettersTest() {
        System.out.println("File Name is:");
        System.out.println(parse.getFileName());
    }

    private static void getCityFromFileTest() {
        CityImpl city = parse.getCityFromFile();
        System.out.println(city);
    }

    private static void getAllCitiesFromFileTest() {
        System.out.println(parse.getAllCitiesFromFile());
    }

    private static void getCitiesNamesTest() {
        for (String name : parse.getCitiesNames()) {
            System.out.println(name);
        }
    }

}
