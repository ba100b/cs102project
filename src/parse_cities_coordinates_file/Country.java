package parse_cities_coordinates_file;
import city_route.core.CityImpl;
import city_route.core.Variables;
import city_route.interfaces.ICity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ub on 20/12/16.
 */
public class Country {

    private static ParseAndList parser = ParseAndList.getInstance();

    public static int numberOfCities;

    public static final List<ICity> CITIES;
    static {
        parser.init(Variables.FILE_NAME);
        CITIES = new LinkedList<>(parser.getAllCitiesFromFile());
        numberOfCities = CITIES.size();
    }

    /**Select the city you want to set off from*/
    public boolean setTargetCity(String cityName) {
        for(ICity iCity : CITIES) {
            if (iCity.getName().equalsIgnoreCase(cityName)) {
                ICity city = new CityImpl();
                city.setName(cityName);
                int index = CITIES.indexOf(city);
                if (index != -1) {
                    Collections.swap(CITIES, 0, index);
                    return true;
                }
            }
        }
        return false;
    }



}
