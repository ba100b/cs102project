package parse_cities_coordinates_file;

import city_route.core.CityImpl;
import city_route.core.GPS;
import city_route.interfaces.ICity;
import city_route.interfaces.ICoordinate3D;
import city_route.interfaces.IParse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ub on 15/12/16.
 */
public class ParseAndList implements IParse {

    private static final double ALT = 1.0;
    private static ParseAndList instance = null;
    private String fileName;
    private CityImpl city;
    private List<ICity> cities;
    private File citiesCoordinatesFile;
    private Scanner readerScanner;

    private ParseAndList() {
    }

    //getters and setters
    //-----------------------------------

    /**
     * You must call init method after instantiating
     */
    public static ParseAndList getInstance() {
        if (instance == null) {
            instance = new ParseAndList();
        }
        return instance;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void setCity(CityImpl city) {
        this.city = city;
    }

    @Override
    public List<ICity> getCities() {
        return cities;
    }

    public void setCities(List cities) {
        this.cities = cities;
    }

    @Override
    public File getCitiesCoordinatesFile() {
        return citiesCoordinatesFile;
    }

    //-----------------------------------
    //Other methods in order of execution

    @Override
    public void setCitiesCoordinatesFile(File citiesCoordinatesFile) {
        this.citiesCoordinatesFile = citiesCoordinatesFile;
    }

    @Override
    public void createFileToRead(String fileName) {
        setFileName(fileName);
        setCitiesCoordinatesFile(new File(getFileName()));
    }

    @Override
    public void makeReaderScanner(File fileToRead) {
        try {
            readerScanner = new Scanner(fileToRead);
        } catch (FileNotFoundException e) {
            System.out.println("The file name is invalid");
            e.printStackTrace();
        }
    }

    @Override
    public CityImpl getCityFromFile() {
        String[] cityDetails;
        String name;
        double lat, lon;
        lat = lon = 0;
        if (readerScanner.hasNext()) {
            cityDetails = readerScanner.next().split(";");
            name = cityDetails[0];
            try {
                lat = Double.parseDouble(cityDetails[1]);
                lon = Double.parseDouble(cityDetails[2]);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.printf("\n %s", "The file format is incorrect");
            }
            ICoordinate3D cityCoordinate = new GPS(lat, lon, ALT);
            return new CityImpl(name, cityCoordinate);
        }
        return null;
    }

    @Override
    public List<ICity> getAllCitiesFromFile() {
        CityImpl city;
        List<ICity> cities = new LinkedList<>();
        while ((city = getCityFromFile()) != null) {
            cities.add(city);
        }
        setCities(cities);
        resetScanner();
        return getCities();
    }

    @Override
    public void resetScanner() {
        readerScanner = null;
        makeReaderScanner(citiesCoordinatesFile);
    }

    public String[] getCitiesNames() {
        String[] citiesNames = new String[Country.CITIES.size()];

        for (int i = 0; i < Country.CITIES.size(); i++) {
            citiesNames[i] = Country.CITIES.get(i).getName();
        }
        return citiesNames;
    }

    @Override
    public void init(String fileName) {
        createFileToRead(fileName);
        makeReaderScanner(getCitiesCoordinatesFile());
    }
}
