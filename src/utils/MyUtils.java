package utils;

import city_route.core.Variables;
import city_route.interfaces.ICity;
import parse_cities_coordinates_file.Country;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ub on 22/12/16.
 */
public class MyUtils {

    private static ArrayList<ICity> dummyList =
            new ArrayList<>(Collections.nCopies(Variables.NUMBER_OF_CITIES, null));

    static {
        dummyList.set(0, Country.CITIES.get(0));
        dummyList.set(dummyList.size() - 1, Country.CITIES.get(0));
    }

    /**
     * This method is created to fill or replace the list elements with dummy values in order
     * to replace items with the specific indexes
     * @param cities : the list you want to fill
     */
    public static void fillDummy(List<ICity> cities) {
        cities.clear();
        cities.addAll(dummyList);
    }


    //test
    /*public static void main(String[] args) {
        System.out.println(dummyList);
    }*/

}
