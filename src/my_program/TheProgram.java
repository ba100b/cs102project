package my_program;

import city_route.core.Route;
import parse_cities_coordinates_file.Country;
import tour_plans.plans.SimplePlan;

import java.util.Scanner;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * Created by ub on 20/12/16.
 */
public class TheProgram {

    private static SimplePlan simplePlan1;
    private static SimplePlan simplePlan2;
    private static SimplePlan simplePlan3;

    private static TreeSet<Route> tree1;
    private static TreeSet<Route> tree2;
    private static TreeSet<Route> tree3;


    public static void main(String[] args) {

        Country country = new Country();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the city you want to sit off from:");
        while (!country.setTargetCity(scanner.nextLine())) {
            System.out.println("Invalid name, please enter the city name again:");
        }

        long startTime = System.currentTimeMillis();

        //Generate random routes, three objects for three threads
        simplePlan1 = new SimplePlan(20);
        simplePlan2 = new SimplePlan(20);
        simplePlan3 = new SimplePlan(20);


        Runnable run1 = new Runnable() {
            @Override
            public void run() {
                tree1 = crossTourTest(1);
            }
        };

        Runnable run2 = new Runnable() {
            @Override
            public void run() {
                tree2 = crossTourTest(2);
            }
        };

        Runnable run3 = new Runnable() {
            @Override
            public void run() {
                tree3 = crossTourTest(3);
            }
        };

        Thread thread1 = new Thread(run1);
        thread1.start();
        Thread thread2 = new Thread(run2);
        thread2.start();
        Thread thread3 = new Thread(run3);
        thread3.start();
        try {
            //Make sure that next lines will not execute until all threads finish
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        TreeSet<Route> finalTree = new TreeSet<>(tree1);
            finalTree.addAll(tree2);
            finalTree.addAll(tree3);

            System.out.println("\n\n\n\n\n\n");
            int counter = 0;
            System.out.println("Final result: ");
            for (Route route : finalTree) {
                if(counter > 4)
                    break;
                System.out.printf("Length: %f, \t%s\n", route.getLength(), route);
                counter++;
            }


            //print time
            long finishTime = System.currentTimeMillis();
            long totalTime = finishTime - startTime;
            System.out.printf("\n\nTime needed in ms: %d\n", totalTime);
            //totalTime = totalTime/1000;
            System.out.printf("\nTime needed in Seconds: %d\n",
                    TimeUnit.MILLISECONDS.toSeconds(totalTime));
            System.out.printf("\nTime needed in Minutes: %d\n",
                    TimeUnit.MILLISECONDS.toMinutes(totalTime));

    }

    public static TreeSet<Route> crossTourTest(int i) {
        TreeSet<Route> routeTreeSet;
        if (i == 1)
            routeTreeSet = new TreeSet<>(simplePlan1.crossTours(30000));
        else if(i == 2 )
            routeTreeSet = new TreeSet<>(simplePlan2.crossTours(30000));
        else
            routeTreeSet = new TreeSet<>(simplePlan3.crossTours(30000));
        return routeTreeSet;
    }

}
